package ru.mirsaitov.tm.entity;

public class Project {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    public Project() {
    }

    public Project(String name) {
        this.name = name;
        this.description = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
