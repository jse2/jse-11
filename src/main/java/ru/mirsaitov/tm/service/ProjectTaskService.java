package ru.mirsaitov.tm.service;

import ru.mirsaitov.tm.constant.TerminalConst;
import ru.mirsaitov.tm.entity.Project;
import ru.mirsaitov.tm.entity.Task;
import ru.mirsaitov.tm.repository.ProjectRepository;
import ru.mirsaitov.tm.repository.TaskRepository;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Add task to project
     *
     * @param projectId - projectId
     * @param taskId - taskId
     */
    public Task addTaskToProject(final Long projectId, final Long taskId) {
        Project project = projectRepository.findById(projectId);
        if (project == null) {
            return null;
        }
        Task task = taskRepository.findById(taskId);
        if (task == null) {
            return null;
        }
        task.setProjectId(project.getId());
        return task;
    }

    /**
     * Remove project
     *
     * @param option - option of deletion
     * @param param - parameter
     * @param deleteTask - delete related task
     */
    public Project removeProject(final String option, final String param, final boolean deleteTask) {
        if (option == null || option.isEmpty()) {
            return null;
        }
        if (param == null || param.isEmpty()) {
            return null;
        }
        Project project = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                project = projectRepository.removeByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_NAME:
                project = projectRepository.removeByName(param);
                break;
            case TerminalConst.OPTION_ID:
                project = projectRepository.removeById(Long.parseLong(param));
                break;
        }
        if (project == null) {
            return null;
        }
        for (final Task task: taskRepository.findTaskByProjectId(project.getId())) {
            if (deleteTask) {
                taskRepository.removeById(task.getId());
            } else {
                task.setProjectId(null);
            }
        }
        return project;
    }

}
