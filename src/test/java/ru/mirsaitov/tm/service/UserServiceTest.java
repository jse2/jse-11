package ru.mirsaitov.tm.service;

import org.junit.Test;
import ru.mirsaitov.tm.entity.User;
import ru.mirsaitov.tm.enumerated.Role;
import ru.mirsaitov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest {

    private final String login = "login";

    private final String password = "password";

    private final UserService userService = new UserService(new UserRepository());

    @Test
    public void shouldClear() {
        User user = userService.create(login, password);
        assertTrue(!userService.findAll().isEmpty());

        userService.clear();
        assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void shouldFindAll() {
        List<User> users = new ArrayList<>();

        users.add(userService.create(login, password));
        assertTrue(users.equals(userService.findAll()));

        users.add(userService.create("ADMIN", "ADMIN"));
        assertTrue(users.equals(userService.findAll()));
    }

    @Test
    public void shouldFind() {
        User user = userService.create(login, password);
        User admin = userService.create("ADMIN", "ADMIN");

        assertEquals(userService.findByLogin(user.getLogin()), user);
        assertEquals(userService.findById(user.getId()), user);

        assertEquals(userService.findByLogin(admin.getLogin()), admin);
        assertEquals(userService.findById(admin.getId()), admin);

        assertEquals(userService.findByLogin("1"), null);
        assertEquals(userService.findById(1L), null);
    }

    @Test
    public void shouldRemove() {
        User user = userService.create(login, password);
        User admin = userService.create("ADMIN", "ADMIN");

        assertEquals(userService.removeByLogin("1"), null);
        assertEquals(userService.removeById(1L), null);

        assertEquals(userService.removeById(user.getId()), user);
        assertEquals(userService.removeByLogin(admin.getLogin()), admin);

        user = userService.create(login, password);
        admin = userService.create("ADMIN", "ADMIN");

        assertEquals(userService.removeByIndex(1), admin);
        assertEquals(userService.removeByIndex(0), user);
    }

    @Test
    public void shouldUpdate() {
        User user = userService.create(login, password);
        User admin = userService.create("ADMIN", "ADMIN");

        assertEquals(user.getRole(),  Role.USER);
        assertEquals(userService.updateByIndex(0, password, Role.ADMIN, login, login, login), user);
        assertEquals(user.getRole(),  Role.ADMIN);

        assertEquals(admin.getFirstName(), "");
        assertEquals(userService.updateByLogin("ADMIN", password, Role.ADMIN, login, login, login), admin);
        assertEquals(admin.getFirstName(),  login);

        assertEquals(admin.getRole(), Role.ADMIN);
        assertEquals(userService.updateById(admin.getId(), password, Role.USER, login, login, login), admin);
        assertEquals(admin.getRole(),  Role.USER);
    }

    @Test
    public void shouldCreate() {
        User user = userService.create(login, password);

        assertEquals(userService.create(login, password), null);
        assertNotEquals(userService.create("ADMIN", "ADMIN"), null);
    }

}
